import React from 'react';
import PlayerWindow from './components/PlayerWindow';
import DungeonMasterWindow from './components/DungeonMasterWindow';
import { FogProvider } from './contexts/FogContext';
import { NoteProvider } from './contexts/NoteContext';
import { MapProvider } from './contexts/MapContext';
import { ToolProvider } from './contexts/ToolContext';
import { GridProvider } from './contexts/GridContext';

function App() {

  if(typeof ResizeObserver === 'undefined' || typeof BroadcastChannel === 'undefined'){
    return (<p>Sorry, your browser does not support all of the techniques used to build this application. Please use another browser.</p>);
  }

  return (
    <FogProvider>
      <NoteProvider>
        <GridProvider>
          <MapProvider>
            <ToolProvider>
              {document.location.pathname === '/player' ? (<PlayerWindow></PlayerWindow>) : (<DungeonMasterWindow></DungeonMasterWindow>)}
            </ToolProvider>
          </MapProvider>
        </GridProvider>
      </NoteProvider>
    </FogProvider>
  );
}

export default App;

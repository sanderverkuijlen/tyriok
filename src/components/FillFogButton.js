import React from 'react';
import ControlButton from './ControlButton';
import { FogConsumer } from '../contexts/FogContext';

class FillFogButton extends React.Component {

  render() {
    return (
      <FogConsumer>
        {({ fogFill }) => (
          <ControlButton onClick={fogFill}>
            Fill fog
          </ControlButton>
        )}
      </FogConsumer>
    );
  }
}

export default FillFogButton;
import React from 'react';
import { NoteConsumer } from '../contexts/NoteContext';

class NoteLayer extends React.Component {
  render() {
    let styles = { display: 'block', width: '100%', height: '100%' };

    if (this.props.transparent) {
      styles.opacity = 0.5;
    }

    return (
      <NoteConsumer>
        {({ noteImageUrl }) => {
          return noteImageUrl ? (<img src={noteImageUrl} style={styles} alt="notes" className="unselectable"></img>) : null;
        }}
      </NoteConsumer>
    );
  };
}

export default NoteLayer;

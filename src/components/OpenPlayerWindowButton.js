import React from 'react';
import ControlButton from './ControlButton';
import { MapConsumer } from '../contexts/MapContext';
import { FogConsumer } from '../contexts/FogContext';
import { NoteConsumer } from '../contexts/NoteContext';
import { GridConsumer } from '../contexts/GridContext';

class OpenPlayerWindowButton extends React.Component {

  openPlayerWindow = () => {
    return new Promise((resolve) => {
      const playerWindow = window.open('/player');

      const onPlayerWindowLoad = () => {
        playerWindow.removeEventListener('load', onPlayerWindowLoad);
        resolve();
      };

      playerWindow.addEventListener('load', onPlayerWindowLoad);
    })
  };

  render() {
    return (
      <MapConsumer>
        {({ broadcastMapState }) => (
          <FogConsumer>
            {({ fogBroadcastState }) => (
              <NoteConsumer>
                {({ noteBroadcastState }) => (
                  <GridConsumer>
                    {({ gridBroadcastState }) => (
                      <ControlButton
                        onClick={() => {
                          this.openPlayerWindow().then(() => {
                            broadcastMapState();
                            fogBroadcastState();
                            noteBroadcastState();
                            gridBroadcastState();
                          });
                        }}
                      >
                        Open player window
                      </ControlButton>
                    )}
                  </GridConsumer>
                )}
              </NoteConsumer>
            )}
          </FogConsumer>
        )}
      </MapConsumer>
    );
  }
}

export default OpenPlayerWindowButton;
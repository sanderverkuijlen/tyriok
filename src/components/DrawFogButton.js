import React from 'react';
import ControlButton from './ControlButton';
import { FogConsumer } from '../contexts/FogContext';
import { ToolConsumer } from '../contexts/ToolContext';

class DrawFogButton extends React.Component {

  render() {
    return (
      <FogConsumer>
        {({ fogDraw }) => (
          <ToolConsumer>
            {({ selectTool, selectBrush, rectangleBrush, polygonBrush, lineBrush }) => (
              <>
                <ControlButton onClick={() => {
                  selectTool(fogDraw)
                    .then(() => {
                      selectBrush(rectangleBrush);
                    });
                }}>
                  Draw fog (Rectangle)
                </ControlButton>
                <ControlButton onClick={() => {
                  selectTool(fogDraw)
                    .then(() => {
                      selectBrush(polygonBrush);
                    });
                }}>
                  Draw fog (Polygon)
                </ControlButton>
                <ControlButton onClick={() => {
                  selectTool(fogDraw)
                    .then(() => {
                      selectBrush(lineBrush);
                    });
                }}>
                  Draw fog (line)
                </ControlButton>
              </>
            )}
          </ToolConsumer>
        )}
      </FogConsumer>
    );
  }
}

export default DrawFogButton;
import React from 'react';
import Layers from './Layers';
import { MapConsumer } from '../contexts/MapContext';

class Map extends React.Component {
  determineMapElement = (map) => {
    if (map === null) {
      return (<p>Open a map by dropping a file here</p>);
    }
    else if (map.type === 'video') {
      return (
        <video
          key={map.url}
          autoPlay
          loop
          muted={this.props.muted}
          style={{ display: 'block', maxWidth: '100%', maxHeight: '100vh' }}
          className="unselectable"
        >
          <source src={map.url}></source>
        </video>
      );
    }
    else if (map.type === 'image') {
      return (
        <img
          src={map.url}
          alt="background"
          style={{ display: 'block', maxWidth: '100%', maxHeight: '100vh' }}
          className="unselectable"
        ></img>
      );
    }
    else {
      return (<p>Unknown map type: {map.type}</p>);
    }
  };

  render() {
    return (
      <MapConsumer>
        {({ map }) => {
          return (
            <div style={{ display: 'flex', width: '100%', height: '100%' }} className="unselectable">
              <div style={{ position: 'relative', margin: 'auto', maxWidth: '100%', maxHeight: '100%' }}>

                {this.determineMapElement(map)}

                {map ? (
                  <Layers>
                    {this.props.children}
                  </Layers>
                ) : null}
              </div>
            </div>
          )
        }}
      </MapConsumer >
    );
  };
}

export default Map;

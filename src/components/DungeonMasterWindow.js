import React from 'react';
import { MapConsumer } from '../contexts/MapContext';
import Map from './Map';
import FogLayer from './FogLayer';
import DrawingLayer from './DrawingLayer';
import Controls from './Controls';
import NoteLayer from './NoteLayer';
import GridLayer from './GridLayer';

class DungeonMasterWindow extends React.Component {

  onDragOver = (event) => {
    event.stopPropagation();
    event.preventDefault();
  };

  onDrop = (event, loadMap) => {
    event.stopPropagation();
    event.preventDefault();

    loadMap(event.dataTransfer.files[0]);
  };

  render() {
    return (
      <MapConsumer>
        {({ loadMap }) => (
          <div
            onDragOver={this.onDragOver}
            onDrop={(event) => {
              this.onDrop(event, loadMap);
            }}
            style={{ width: '100%', height: '100%' }}
          >
            <Map muted>
              <FogLayer key="fog" transparent={true}></FogLayer>
              <NoteLayer key="notes"></NoteLayer>
              <GridLayer key="grid"></GridLayer>
              <DrawingLayer key="drawing"></DrawingLayer>
            </Map>

            <Controls></Controls>
          </div>
        )}
      </MapConsumer>
    );
  }
}

export default DungeonMasterWindow;
import React from 'react';
import ControlButton from './ControlButton';
import { MapConsumer } from '../contexts/MapContext';
import { FogConsumer } from '../contexts/FogContext';
import { GridConsumer } from '../contexts/GridContext';
import { NoteConsumer } from '../contexts/NoteContext';

class SyncButton extends React.Component {
  render() {
    return (
      <MapConsumer>
        {({ broadcastMapState }) => (
          <FogConsumer>
            {({ fogBroadcastState }) => (
              <NoteConsumer>
                {({ noteBroadcastState }) => (
                  <GridConsumer>
                    {({ gridBroadcastState }) => (
                      <ControlButton
                        onClick={() => {
                          broadcastMapState();
                          fogBroadcastState();
                          noteBroadcastState();
                          gridBroadcastState();
                        }}
                      >
                        Sync
                      </ControlButton>
                    )}
                  </GridConsumer>
                )}
              </NoteConsumer>
            )}
          </FogConsumer>
        )}
      </MapConsumer>
    );
  }
}

export default SyncButton;
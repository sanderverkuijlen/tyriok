import React from 'react';

class Layers extends React.Component {

  render() {
    let renderLayer = (child, childIndex) => {
      return (
        <div key={childIndex} style={{ width: '100%', height: '100%', position: 'absolute', top: 0, left: 0 }}>
          {child}
        </div>
      );
    };

    if (Array.isArray(this.props.children)) {
      return this.props.children.map(renderLayer);
    }
    else {
      return renderLayer(this.props.children);
    }
  };
}

export default Layers;

import React from 'react';
import ControlButton from './ControlButton';
import { MapConsumer } from '../contexts/MapContext';

class LoadMapButton extends React.Component {

  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
  }

  render() {
    return (
      <MapConsumer>
        {({ loadMap }) => (
          <>
            <ControlButton
              onClick={() => {
                if (this.fileInputRef && this.fileInputRef.current) {
                  this.fileInputRef.current.click();
                }
              }}
            >
              Load map
            </ControlButton>
            <input
              ref={this.fileInputRef}
              type="file"
              accept="video/*,image/*"
              onChange={(event) => {
                loadMap(event.target.files[0]);
              }}
              style={{ display: 'none' }}
            ></input>
          </>
        )}
      </MapConsumer>
    );
  }
}

export default LoadMapButton;
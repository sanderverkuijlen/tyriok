import React from 'react';
import { FogConsumer } from '../contexts/FogContext';

class Fog extends React.Component {
  render() {
    let styles = { display: 'block', width: '100%', height: '100%' };

    if (this.props.transparent) {
      styles.opacity = 0.5;
    }

    return (
      <FogConsumer>
        {({ fogImageUrl }) => {
          return fogImageUrl ? (<img src={fogImageUrl} style={styles} alt="fog" className="unselectable"></img>) : null;
        }}
      </FogConsumer>
    );
  };
}

export default Fog;

import React from 'react';
import ControlButton from './ControlButton';
import {FogConsumer} from '../contexts/FogContext';

class ClearFogButton extends React.Component {

  render() {
    return (
      <FogConsumer>
        {({ fogClear }) => (
          <ControlButton onClick={fogClear}>
            Clear fog
          </ControlButton>
        )}
      </FogConsumer>
    );
  }
}

export default ClearFogButton;
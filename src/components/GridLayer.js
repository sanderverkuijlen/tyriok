import React from 'react';
import { GridConsumer } from '../contexts/GridContext';

function numberToColumnHeader(number){
  var string = '', t;

  while (number > 0) {
    t = (number - 1) % 26;
    string = String.fromCharCode(65 + t) + string;
    number = (number - t)/26 | 0;
  }
  return string || undefined;
}

class GridCanvas extends React.Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
  }

  redrawGrid = () => {
    const canvas = this.canvasRef.current;
    const context = canvas.getContext('2d');

    const gridWidth = canvas.scrollWidth;
    const gridHeight = canvas.scrollHeight;
    const gridSize = gridWidth / parseInt(this.props.size, 10);
    const gridNumberX = gridWidth / gridSize;
    const gridNumberY = gridHeight / gridSize;

    canvas.width = gridWidth;
    canvas.height = gridHeight;

    context.clearRect(0, 0, gridWidth, gridHeight);

    if(this.props.color === "black"){
      context.strokeStyle = 'rgba(0,0,0,0.5)';
      context.fillStyle = 'rgba(0,0,0,1)';
    }
    else{
      context.strokeStyle = 'rgba(255,255,255,0.10)';
      context.fillStyle = 'rgba(255,255,255,0.2)';
    }

    context.lineWidth = 1;

    context.textAlign = "center";
    context.textBaseline = "middle";
    context.font = (gridSize*0.6)+"px serif";

    if (this.props.enabled) {
      for (let indexX = 0; indexX <= gridNumberX; indexX++) {
        context.beginPath();
        context.moveTo(indexX * gridSize, 0);
        context.lineTo(indexX * gridSize, gridHeight);
        context.stroke();

        context.fillText(numberToColumnHeader(indexX+1) + (indexX === 0 ? "0" : ""), (indexX * gridSize) + (gridSize/2), gridSize * 0.5);
      }
      for (let indexY = 0; indexY <= gridNumberY; indexY++) {
        context.beginPath();
        context.moveTo(0, indexY * gridSize);
        context.lineTo(gridWidth, indexY * gridSize);
        context.stroke();

        if(indexY > 0){
          context.fillText(indexY, gridSize * 0.5, (indexY * gridSize) + (gridSize/1.85));
        }
      }
    }
  };

  resizeObserver = new ResizeObserver(this.redrawGrid);

  componentDidMount() {
    this.resizeObserver.observe(this.canvasRef.current);
  }

  componentWillUnmount() {
    this.resizeObserver.disconnect();
  }

  componentDidUpdate() {
    this.redrawGrid();
  }

  render() {
    let styles = { display: 'block', width: '100%', height: '100%' };
    return <canvas ref={this.canvasRef} style={styles}></canvas>;
  };
}

class GridLayer extends React.Component {
  render() {
    return (
      <GridConsumer>
        {({ gridEnabled, gridSize, gridColor }) => {
          return (<GridCanvas enabled={gridEnabled} size={gridSize} color={gridColor}></GridCanvas>);
        }}
      </GridConsumer>
    );
  };
}

export default GridLayer;

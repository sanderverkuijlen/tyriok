import React from 'react';
import Map from './Map';
import FogLayer from './FogLayer';
import NoteLayer from './NoteLayer';
import GridLayer from './GridLayer';

class PlayerWindow extends React.Component {
  render() {
    return (
      <Map>
        <FogLayer></FogLayer>
        <NoteLayer></NoteLayer>
        <GridLayer></GridLayer>
      </Map>
    );
  }
}

export default PlayerWindow;

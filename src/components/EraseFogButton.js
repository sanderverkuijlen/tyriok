import React from 'react';
import ControlButton from './ControlButton';
import { FogConsumer } from '../contexts/FogContext';
import { ToolConsumer } from '../contexts/ToolContext';

class EraseFogButton extends React.Component {

  render() {
    return (
      <FogConsumer>
        {({ fogErase }) => (
          <ToolConsumer>
            {({ selectTool, selectBrush, rectangleBrush, polygonBrush, lineBrush }) => (
              <>
                <ControlButton onClick={() => {
                  selectTool(fogErase)
                    .then(() => {
                      selectBrush(rectangleBrush);
                    });
                }}>
                  Erase fog (Rectangle)
                </ControlButton>
                <ControlButton onClick={() => {
                  selectTool(fogErase)
                    .then(() => {
                      selectBrush(polygonBrush);
                    });
                }}>
                  Erase fog (Polygon)
                </ControlButton>
                <ControlButton onClick={() => {
                  selectTool(fogErase)
                    .then(() => {
                      selectBrush(lineBrush);
                    });
                }}>
                  Erase fog (line)
                </ControlButton>
              </>
            )}
          </ToolConsumer>
        )}
      </FogConsumer>
    );
  }
}

export default EraseFogButton;
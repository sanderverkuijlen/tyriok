import React from 'react';
import throttle from '../functions/throttle';
import { ToolConsumer } from '../contexts/ToolContext';

class DrawingLayer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentTool: null,
      currentBrush: null,
      currentColor: null,
      bounding: null,
      points: null,
    }

    this.canvasRef = React.createRef();
  }

  redrawPreview = (brush, color, points, mapWidth, mapHeight) => {
    const context = this.canvasRef.current.getContext('2d');

    context.canvas.width = mapWidth;
    context.canvas.height = mapHeight;

    context.clearRect(0, 0, context.canvas.width, context.canvas.height);

    context.strokeStyle = "rgba(255,0,0, 0.25)";
    context.fillStyle = "rgba(255,0,0, 0.25)";

    if (points !== null) {
      brush(context, color, points);
    }
  };

  mouseDown = (event, tool, brush, color) => {
    if (tool && brush) {
      const bounding = event.target.getBoundingClientRect();

      window.addEventListener('mouseup', this.mouseUp);
      window.addEventListener('mousemove', this.mouseMove);

      this.setState({
        ...this.state,
        currentTool: tool,
        currentBrush: brush,
        currentColor: color,
        bounding: bounding,
        points: [{
          x: event.clientX - bounding.x,
          y: event.clientY - bounding.y,
        }],
      });
    }
  };

  mouseMove = throttle((event) => {
    let points = this.state.points;
    points.push({
      x: event.clientX - this.state.bounding.x,
      y: event.clientY - this.state.bounding.y,
    });

    this.setState({
      ...this.state,
      points: points,
    });

    this.redrawPreview(this.state.currentBrush, this.state.currentColor, points, this.state.bounding.width, this.state.bounding.height);
  }, 10);

  mouseUp = (event) => {
    let points = this.state.points;
    points.push({
      x: event.clientX - this.state.bounding.x,
      y: event.clientY - this.state.bounding.y,
    });

    this.state.currentTool(this.state.currentBrush, this.state.currentColor, points, this.state.bounding.width, this.state.bounding.height);

    window.removeEventListener('mouseup', this.mouseUp);
    window.removeEventListener('mousemove', this.mouseMove);

    this.setState({
      ...this.state,
      currentTool: null,
      currentBrush: null,
      currentColor: null,
      bounding: null,
      points: null,
    });

    this.redrawPreview(null, null, null, null);
  };

  render() {
    let styles = { display: 'block', width: '100%', height: '100%' };

    return (
      <ToolConsumer>
        {({ selectedTool, selectedBrush, selectedColor }) => (
          <canvas
            ref={this.canvasRef}
            onMouseDown={(event) => { this.mouseDown(event, selectedTool, selectedBrush, selectedColor); }}
            style={styles}
            className="unselectable"
          ></canvas>
        )}
      </ToolConsumer>
    );
  };
}

export default DrawingLayer;

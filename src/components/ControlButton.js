import React from 'react';

class ControlButton extends React.Component {

  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
  }

  render() {
    let {
      style,
      ...otherProps
    } = this.props;

    style = {
      display: 'inline-block',
      border: 'none',
      background: 'none',
      cursor: 'pointer',
      color: 'white',
      padding: 0,
      margin: '0 5px',
      outline: 'none',
      ...this.props.style,
    };

    return (
      <button
        style={style}
        {...otherProps}
      >
        {this.props.children}
      </button>
    );
  }
}

export default ControlButton;
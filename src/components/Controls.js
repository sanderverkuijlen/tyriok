import React from 'react';
import ControlButton from './ControlButton';
import OpenPlayerWindowButton from './OpenPlayerWindowButton';
import LoadMapButton from './LoadMapButton';
import SyncButton from './SyncButton';
import { FogConsumer } from '../contexts/FogContext';
import { GridConsumer } from '../contexts/GridContext';
import { NoteConsumer } from '../contexts/NoteContext';
import { ToolConsumer } from '../contexts/ToolContext';

class ControlGroup extends React.Component {
  render() {
    return (
      <div style={{
        float: 'left',
        display: 'inline-block',
      }}>
        <p>{this.props.label}</p>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  };
}

class Controls extends React.Component {
  render() {

    const buttonStyle = (selected, selectee) => {
      let style = {};
      if (selectee === selected) {
        style = {
          ...style,
          color: 'yellow',
        };
      }

      return style;
    };

    return (
      <FogConsumer>
        {({ fogClear, fogFill, fogErase, fogDraw }) => (
          <NoteConsumer>
            {({ noteClear, noteErase, noteDraw }) => (
              <ToolConsumer>
                {({ selectedTool, selectedBrush, selectedColor, selectTool, selectBrush, selectColor, rectangleBrush, circleBrush, polygonBrush, lineBrush, blueColor, purpleColor, redColor, orangeColor, yellowColor, greenColor }) => (
                  <div
                    style={{
                      position: 'fixed',
                      top: 0,
                      left: 0,
                    }}
                  >
                    <ControlGroup label="Generic">
                      <OpenPlayerWindowButton>
                        Open player window
                      </OpenPlayerWindowButton>
                      <LoadMapButton>
                        Load map
                      </LoadMapButton>
                      <SyncButton></SyncButton>
                    </ControlGroup>

                    <ControlGroup label="Fog">
                      <ControlButton title="Clear fog" onClick={fogClear}>
                        Clear
                      </ControlButton>
                      <ControlButton title="Fill fog" onClick={fogFill}>
                        Fill
                      </ControlButton>
                      <ControlButton title="Erase fog" onClick={() => { selectTool(fogErase) }} style={buttonStyle(selectedTool, fogErase)}>
                        Erase
                      </ControlButton>
                      <ControlButton title="Draw fog" onClick={() => { selectTool(fogDraw) }} style={buttonStyle(selectedTool, fogDraw)}>
                        Draw
                      </ControlButton>
                    </ControlGroup>

                    <ControlGroup label="Notes">
                      <ControlButton title="Clear notes" onClick={noteClear}>
                        Clear
                      </ControlButton>
                      <ControlButton title="Erase notes" onClick={() => { selectTool(noteErase) }} style={buttonStyle(selectedTool, noteErase)}>
                        Erase
                      </ControlButton>
                      <ControlButton title="Draw notes" onClick={() => { selectTool(noteDraw) }} style={buttonStyle(selectedTool, noteDraw)}>
                        Draw
                      </ControlButton>
                    </ControlGroup>

                    <ControlGroup label="Brushes">
                      <ControlButton title="Rectangle" onClick={() => { selectBrush(rectangleBrush) }} style={buttonStyle(selectedBrush, rectangleBrush)}>
                        Rectangle
                      </ControlButton>
                      <ControlButton title="Circle" onClick={() => { selectBrush(circleBrush) }} style={buttonStyle(selectedBrush, circleBrush)}>
                        Circle
                      </ControlButton>
                      <ControlButton title="Polygon" onClick={() => { selectBrush(polygonBrush) }} style={buttonStyle(selectedBrush, polygonBrush)}>
                        Polygon
                      </ControlButton>
                      <ControlButton title="Line" onClick={() => { selectBrush(lineBrush) }} style={buttonStyle(selectedBrush, lineBrush)}>
                        Line
                      </ControlButton>
                    </ControlGroup>

                    <ControlGroup label="Colors">
                      <ControlButton title="Blue" onClick={() => { selectColor(blueColor) }} style={buttonStyle(selectedColor, blueColor)}>
                        Blue
                      </ControlButton>
                      <ControlButton title="Purple" onClick={() => { selectColor(purpleColor) }} style={buttonStyle(selectedColor, purpleColor)}>
                        Purple
                      </ControlButton>
                      <ControlButton title="Red" onClick={() => { selectColor(redColor) }} style={buttonStyle(selectedColor, redColor)}>
                        Red
                      </ControlButton>
                      <ControlButton title="Orange" onClick={() => { selectColor(orangeColor) }} style={buttonStyle(selectedColor, orangeColor)}>
                        Orange
                      </ControlButton>
                      <ControlButton title="Yellow" onClick={() => { selectColor(yellowColor) }} style={buttonStyle(selectedColor, yellowColor)}>
                        Yellow
                      </ControlButton>
                      <ControlButton title="Green" onClick={() => { selectColor(greenColor) }} style={buttonStyle(selectedColor, greenColor)}>
                        Green
                      </ControlButton>
                    </ControlGroup>

                    <ControlGroup label="Grid">
                      <GridConsumer>
                        {({ gridEnabled, gridSize, gridColor, setGridEnabled, setGridSize, setGridColor }) => (
                          <>
                            <input type="checkbox" checked={gridEnabled} onChange={(event) => { setGridEnabled(event.target.checked) }}></input>
                            <input type="number" value={gridSize ? gridSize : null} onChange={(event) => { setGridSize(parseFloat(event.target.value ? event.target.value : 0)); }} min={10} max={100}></input>
                            <select value={gridColor} onChange={(event) => { setGridColor(event.target.value) }}>
                              <option value="black">Black</option>
                              <option value="white">White</option>
                            </select>
                          </>
                        )}
                      </GridConsumer>
                    </ControlGroup>
                  </div>
                )}
              </ToolConsumer>
            )}
          </NoteConsumer>
        )}
      </FogConsumer>
    );
  }
}

export default Controls;
import React from "react";

const FogContext = React.createContext();

const canvas = document.createElement('canvas');
const context = canvas.getContext('2d');

export class FogProvider extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      imageUrl: null,
    };

    this.BroadcastChannel = new BroadcastChannel('fog_state');
    this.BroadcastChannel.onmessage = (event) => {
      this.setState(event.data);
      this.syncStateToCanvas(event.data.imageUrl);
    };
  }

  componentWillUnmount() {
    this.BroadcastChannel.close();
  }

  broadcastState = () => {
    this.BroadcastChannel.postMessage(this.state);
  }

  resizeCanvas = (mapWidth, mapHeight) => {
    return new Promise((resolve) => {
      if (canvas.width !== mapWidth || canvas.height !== mapHeight) {
        const image = document.createElement('img');
        image.onload = () => {
          
          context.canvas.width = mapWidth;
          context.canvas.height = mapHeight;
          context.drawImage(image, 0, 0, canvas.width, canvas.height);

          resolve();
        };
        image.src = canvas.toDataURL();
      }
      else {
        resolve();
      }
    });
  }

  syncCanvasToState = () => {
    return new Promise((resolve) => {
      canvas.toBlob(function (blob) {
        resolve(blob);
      })
    }).then((blob) => {

      if (this.state.imageUrl) {
        window.URL.revokeObjectURL(this.state.imageUrl);
      }

      const newState = {
        ...this.state,
        imageUrl: window.URL.createObjectURL(blob),
      };

      this.setState(newState);
      this.BroadcastChannel.postMessage(newState);
    });
  };

  syncStateToCanvas = (imageUrl) => {
    return new Promise((resolve) => {
      context.clearRect(0, 0, canvas.width, canvas.height);

      if (imageUrl) {
        const imageElement = document.createElement('img');
        imageElement.onload = () => {
          context.drawImage(imageElement, 0, 0, canvas.width, canvas.height);
          resolve();

        };
        imageElement.src = imageUrl;
      }
      else {
        resolve();
      }
    });
  }

  fill = () => {
    return new Promise((resolve) => {

      context.fillStyle = 'black';
      context.fillRect(0, 0, canvas.width, canvas.height);

      resolve();

    }).then(() => {
      return this.syncCanvasToState();
    });
  };

  clear = () => {
    return new Promise((resolve) => {

      context.clearRect(0, 0, canvas.width, canvas.height);
      resolve();

    }).then(() => {
      return this.syncCanvasToState();
    });
  };

  draw = (brush, color, points, mapWidth, mapHeight) => {
    return this.resizeCanvas(mapWidth, mapHeight)
      .then(() => {
        
        brush(context, 'black', points);

        return this.syncCanvasToState();
      });
  };

  erase = (brush, color, points, mapWidth, mapHeight) => {
    return this.resizeCanvas(mapWidth, mapHeight)
      .then(() => {
        
        context.globalCompositeOperation="destination-out";
        brush(context, 'black', points);
        context.globalCompositeOperation="source-over";

        return this.syncCanvasToState();
      });
  };

  render() {
    return (
      <FogContext.Provider
        value={{
          fogImageUrl: this.state.imageUrl,
          fogBroadcastState: this.broadcastState,
          fogFill: this.fill,
          fogClear: this.clear,
          fogDraw: this.draw,
          fogErase: this.erase,
        }}
      >
        {this.props.children}
      </FogContext.Provider>
    );
  }
}
export const FogConsumer = FogContext.Consumer;
export default FogContext;
import React from "react";
import FogContext from '../contexts/FogContext';

const ToolContext = React.createContext();

const drawPoints = (canvasContext, points) => {
  canvasContext.lineJoin = 'round';
  canvasContext.lineCap = 'round';

  canvasContext.beginPath();

  for (let pointIndex = 0; pointIndex < points.length; pointIndex++) {
    if (pointIndex === 0) {
      canvasContext.moveTo(points[pointIndex].x, points[pointIndex].y);
    }
    else {
      canvasContext.lineTo(points[pointIndex].x, points[pointIndex].y);
    }
  }
}

export class ToolProvider extends React.Component {
  static contextType = FogContext;

  constructor(props) {
    super(props);

    this.state = {
      selectedTool: null,
      selectedBrush: this.rectangleBrush,
      selectedColor: this.redColor,
    };
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      selectedTool: this.context.fogErase,
    });
  }

  selectTool = (tool) => {
    return new Promise((resolve) => {
      this.setState({
        ...this.state,
        selectedTool: tool,
      });
      resolve();
    });
  };

  selectBrush = (brush) => {
    return new Promise((resolve) => {
      this.setState({
        ...this.state,
        selectedBrush: brush,
      });
      resolve();
    });
  };

  selectColor = (color) => {
    return new Promise((resolve) => {
      this.setState({
        ...this.state,
        selectedColor: color,
      });
      resolve();
    });
  };

  rectangleBrush = (canvasContext, color, points) => {
    const lastPointIndex = points.length - 1;
    canvasContext.fillStyle = color;
    canvasContext.fillRect(points[0].x, points[0].y, points[lastPointIndex].x - points[0].x, points[lastPointIndex].y - points[0].y);
  };

  circleBrush = (canvasContext, color, points) => {
    const lastPointIndex = points.length - 1;
    const radius = Math.hypot(points[lastPointIndex].x - points[0].x, points[lastPointIndex].y - points[0].y);
    canvasContext.fillStyle = color;
    canvasContext.beginPath();
    canvasContext.arc(points[0].x, points[0].y, radius, 0, 2 * Math.PI);
    canvasContext.fill();
  };

  polygonBrush = (canvasContext, color, points) => {
    canvasContext.fillStyle = color;
    drawPoints(canvasContext, points);
    canvasContext.fill();
  };

  lineBrush = (canvasContext, color, points) => {
    canvasContext.strokeStyle = color;
    canvasContext.lineWidth = Math.min(canvasContext.canvas.width, canvasContext.canvas.height) / 10;
    drawPoints(canvasContext, points);
    canvasContext.stroke();
  };

  blueColor = 'rgba(0,0,255,0.25)';
  purpleColor = 'rgba(255,0,255,0.25)';
  redColor = 'rgba(255,0,0,0.25)';
  orangeColor = 'rgba(255,165,0,0.25)';
  yellowColor = 'rgba(255,255,0,0.25)';
  greenColor = 'rgba(0,255,0,0.25)';

  render() {
    return (
      <ToolContext.Provider
        value={{
          selectedTool: this.state.selectedTool,
          selectedBrush: this.state.selectedBrush,
          selectedColor: this.state.selectedColor,

          selectTool: this.selectTool,
          selectBrush: this.selectBrush,
          selectColor: this.selectColor,

          lineBrush: this.lineBrush,
          rectangleBrush: this.rectangleBrush,
          polygonBrush: this.polygonBrush,
          circleBrush: this.circleBrush,

          blueColor: this.blueColor,
          purpleColor: this.purpleColor,
          redColor: this.redColor,
          orangeColor: this.orangeColor,
          yellowColor: this.yellowColor,
          greenColor: this.greenColor,
        }}
      >
        {this.props.children}
      </ToolContext.Provider>
    );
  }
}
export const ToolConsumer = ToolContext.Consumer;
export default ToolContext;
import React from "react";

const NoteContext = React.createContext();

const canvas = document.createElement('canvas');
const context = canvas.getContext('2d');

export class NoteProvider extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      imageUrl: null,
    };

    this.BroadcastChannel = new BroadcastChannel('note_state');
    this.BroadcastChannel.onmessage = (event) => {
      this.setState(event.data);
      this.syncStateToCanvas(event.data.imageUrl);
    };
  }

  componentWillUnmount() {
    this.BroadcastChannel.close();
  }

  broadcastState = () => {
    this.BroadcastChannel.postMessage(this.state);
  }

  resizeCanvas = (mapWidth, mapHeight) => {
    return new Promise((resolve) => {
      if (canvas.width !== mapWidth || canvas.height !== mapHeight) {
        const image = document.createElement('img');
        image.onload = () => {
          
          context.canvas.width = mapWidth;
          context.canvas.height = mapHeight;
          context.drawImage(image, 0, 0, canvas.width, canvas.height);

          resolve();
        };
        image.src = canvas.toDataURL();
      }
      else {
        resolve();
      }
    });
  }

  syncCanvasToState = () => {
    return new Promise((resolve) => {
      canvas.toBlob(function (blob) {
        resolve(blob);
      })
    }).then((blob) => {

      if (this.state.imageUrl) {
        window.URL.revokeObjectURL(this.state.imageUrl);
      }

      const newState = {
        ...this.state,
        imageUrl: window.URL.createObjectURL(blob),
      };

      this.setState(newState);
      this.BroadcastChannel.postMessage(newState);
    });
  };

  syncStateToCanvas = (imageUrl) => {
    return new Promise((resolve) => {
      context.clearRect(0, 0, canvas.width, canvas.height);

      if (imageUrl) {
        const imageElement = document.createElement('img');
        imageElement.onload = () => {
          context.drawImage(imageElement, 0, 0, canvas.width, canvas.height);
          resolve();

        };
        imageElement.src = imageUrl;
      }
      else {
        resolve();
      }
    });
  }

  clear = () => {
    return new Promise((resolve) => {

      context.clearRect(0, 0, canvas.width, canvas.height);
      resolve();

    }).then(() => {
      return this.syncCanvasToState();
    });
  };

  draw = (brush, color, points, mapWidth, mapHeight) => {
    return this.resizeCanvas(mapWidth, mapHeight)
      .then(() => {
        
        context.globalCompositeOperation="destination-out";
        brush(context, 'black', points);
        context.globalCompositeOperation="source-over";
        brush(context, color, points);

        return this.syncCanvasToState();
      });
  };

  erase = (brush, color, points, mapWidth, mapHeight) => {
    return this.resizeCanvas(mapWidth, mapHeight)
      .then(() => {
        
        context.globalCompositeOperation="destination-out";
        brush(context, 'black', points);
        context.globalCompositeOperation="source-over";

        return this.syncCanvasToState();
      });
  };

  render() {
    return (
      <NoteContext.Provider
        value={{
          noteImageUrl: this.state.imageUrl,
          noteBroadcastState: this.broadcastState,
          noteClear: this.clear,
          noteDraw: this.draw,
          noteErase: this.erase,
        }}
      >
        {this.props.children}
      </NoteContext.Provider>
    );
  }
}
export const NoteConsumer = NoteContext.Consumer;
export default NoteContext;
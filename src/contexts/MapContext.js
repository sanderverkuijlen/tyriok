import React from "react";
import { FogConsumer } from './FogContext';
import { NoteConsumer } from './NoteContext';

const MapContext = React.createContext();

export class MapProvider extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      map: null,
    };

    this.BroadcastChannel = new BroadcastChannel('map_state');
    this.BroadcastChannel.onmessage = (event) => {
      this.setState(event.data);
    };
  }

  isValidFile = (file) => {
    if (typeof file === 'undefined') {
      return false;
    }

    const type = this.getTypeFromFile(file);
    return type === 'image' || type === 'video';
  }

  getTypeFromFile = (file) => {
    const fileTypeMatch = file.type.match(/^([a-z]+)\//i);

    if (!fileTypeMatch || fileTypeMatch.length <= 1) {
      return null;
    }

    return fileTypeMatch[1];
  }

  componentWillUnmount() {
    this.BroadcastChannel.close();
  }

  broadcastMapState = () => {
    this.BroadcastChannel.postMessage(this.state);
  };

  loadMap = (file) => {
    return new Promise((resolve, reject) => {

      if (!this.isValidFile(file)) {
        reject('This does not appear to be an image or video, please select a different file');
      }

      if (this.state.map) {
        window.URL.revokeObjectURL(this.state.map.url);
      }

      let backgroundUrl = window.URL.createObjectURL(file);
      let backgroundType = this.getTypeFromFile(file);

      let newState = {
        ...this.state,
        map: {
          url: backgroundUrl,
          type: backgroundType,
        },
      };

      this.setState(newState);
      this.BroadcastChannel.postMessage(newState);

      resolve();
    });
  };

  render() {
    return (
      <FogConsumer>
        {({ fogFill }) => (
          <NoteConsumer>
            {({ noteClear }) => (
              <MapContext.Provider
                value={{
                  map: this.state.map,
                  loadMap: (file) => {
                    this.loadMap(file)
                      .then(() => {
                        fogFill();
                      })
                      .then(() => {
                        noteClear();
                      });
                  },
                  broadcastMapState: this.broadcastMapState,
                }}
              >
                {this.props.children}
              </MapContext.Provider>
            )}
          </NoteConsumer>
        )}
      </FogConsumer>
    );
  }
}
export const MapConsumer = MapContext.Consumer;
export default MapContext;
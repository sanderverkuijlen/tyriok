import React from "react";

const GridContext = React.createContext();

export class GridProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      gridEnabled: true,
      gridSize: 35,
      gridColor: "black",
    };

    this.BroadcastChannel = new BroadcastChannel('grid_state');
    this.BroadcastChannel.onmessage = (event) => {
      this.setState(event.data);
    };
  }

  componentWillUnmount() {
    this.BroadcastChannel.close();
  }

  broadcastState = () => {
    this.BroadcastChannel.postMessage(this.state);
  }

  setGridEnabled = (gridEnabled) => {
    const newState = {
      ...this.state,
      gridEnabled: gridEnabled,
    };

    this.setState(newState);
    this.BroadcastChannel.postMessage(newState);
  };

  setGridSize = (gridSize) => {
    const newState = {
      ...this.state,
      gridSize: gridSize,
    };

    this.setState(newState);
    this.BroadcastChannel.postMessage(newState);
  };

  setGridColor = (gridColor) => {
    const newState = {
      ...this.state,
      gridColor: gridColor,
    };

    this.setState(newState);
    this.BroadcastChannel.postMessage(newState);
  };

  render() {
    return (
      <GridContext.Provider
        value={{
          gridEnabled: this.state.gridEnabled,
          gridSize: this.state.gridSize,
          gridColor: this.state.gridColor,
          setGridEnabled: this.setGridEnabled,
          setGridSize: this.setGridSize,
          setGridColor: this.setGridColor,

          gridBroadcastState: this.broadcastState,
        }}
      >
        {this.props.children}
      </GridContext.Provider>
    );
  }
}
export const GridConsumer = GridContext.Consumer;
export default GridContext;